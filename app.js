import { Car } from "./car.js";
import { Retangle } from "./retangle.js";
import { Square } from "./square.js";
// Khởi tạo các thực thể
var car1 = new Car("BMW", 2018);
var car2 = new Car("Vinfast", 2019);

console.log(car1.getName());
console.log(car2.getName());

car1.setName("BMW I8");

console.log(car1.getName());

console.log(car1.getAge());
console.log(car2.getAge());

var car3 = new Car();
console.log(car3.getName());
car3.setName("Volvo");
console.log(car3.getName());



var retangle1 = new Retangle(5, 10);
var retangle2 = new Retangle(10, 20);

console.log(retangle1.getArea());
console.log(retangle2.getArea());

var square1 = new Square(5);
var square2 = new Square(10, "Hình vuông");

console.log(square1.getArea());
console.log(square2.getArea());

console.log(square1.getDescription());
console.log(square2.getDescription());

console.log(square1.getPerimeter());
console.log(square2.getPerimeter());

console.log(retangle1 instanceof Retangle);
console.log(retangle1 instanceof Square);
console.log(square1 instanceof Retangle);
console.log(square1 instanceof Square);

// Hoisting
console.log(sumNumber(1,2));
function sumNumber (a,b) {
    return  a+b;
};

