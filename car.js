class Car {
    // Khai báo thuộc tính
    _name;
    _year;

    // Khai báo phương thức khởi tạo
    constructor(paramName = "Default", paramYear = 0) {
        this._name = paramName;
        this._year = paramYear;
    }

    // Phương thức getter & setter
    //getter
    getName() {
        return this._name;
    }

    //setter
    setName(paramName) {
        this._name = paramName;
    }

    // Phương thức khác
    getAge() {
        let today = new Date();

        return today.getFullYear() - this._year;
    }
}

export { Car }